#include "Functions.h"

void Test_Display()
{
    cout << endl << string( 20 , '-' ) << " Test_Display (Manual tests) " << string( 20, '-') << endl;
    { // test begin
        cout << "Test 1 - Classes" << endl;
        vector<string> header = { "Class 1", "Class 2", "Class 3", "Class 4" };
        vector<vector<string>> data = {
            { "CS 134", "CS 200", "CS 235", "CS 250" },
            { "ASL 120", "ASL 122", "ASL 135", "ASL 145" }
        };

        Display( header, data );
    } // test end

    cout << endl << endl;

    { // test begin
        cout << "Test 2 - Minimum wage vs. Median monthly rent" << endl;
        vector<string> header = { "2010", "2000", "1990", "1980", "1970", "1960", "1950" };
        vector<vector<float>> data = {
            { 7.25, 5.15, 3.80, 3.10, 1.60, 1.00, 0.75 },
            { 841,  602,  447,  243,  108,  71,   42  },
        };

        Display( header, data );
    } // test end
}

void Test_Add()
{
    //test 1
    int val1 = 3;
    int val2 = 23;
    int expected_val = 26;
    int actual_val = Add(val1, val2);
    string result = ""; //Used in if-statement^^. Assigns the string into result to use in an cout statement later&&

    cout << endl << string(20, '-') << " Test_Add (Manual tests) " << string(20, '-') << endl;
    cout << endl;

    if (expected_val != actual_val)
    {
        result = "[FAIL]";//^^
    }
    else
    {
        result = "[PASS]";//^^
    }
    cout << result;//&&
    cout << " Test 1 - Integers" << endl;
    cout << endl; 
    cout << "Integer #1: " << val1 << endl;
    cout << "Integer #2: " << val2 << endl;
    cout << endl;
    cout << "Expected Value: " << expected_val << endl;
    cout << "Actual Value: " << actual_val << endl; 
    cout << endl;

    //test 2
    float fl1 = 7.88;
    float fl2 = 9.22;
    float expected_fl = 1000;
    float actual_fl = Add(fl1, fl2);

    cout << endl << string(60, '-') << endl;
    cout << endl;

    if (expected_val != actual_val)
    {
        result = "[FAIL]";//^^
    }
    else
    {
        result = "[PASS]";//^^
    }
    cout << result;
    cout << " Test 2 - Integers" << endl;
    cout << endl;
    cout << "Integer #1: " << fl1 << endl;
    cout << "Integer #2: " << fl2 << endl;
    cout << endl;
    cout << "Expected Value: " << expected_fl << endl;
    cout << "Actual Value: " << actual_fl << endl;
    cout << endl;

   

}

